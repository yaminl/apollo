/******************************************************************************
 * 扩展卡尔曼滤波器
 *****************************************************************************/

#ifndef MODULES_PERCEPTION_OBSTACLE_CAMERA_FILTER_OBJECT_CAMERA_KALMAN_FILTER_H_
#define MODULES_PERCEPTION_OBSTACLE_CAMERA_FILTER_OBJECT_CAMERA_KALMAN_FILTER_H_

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "modules/common/math/extended_kalman_filter.h" // 扩展卡尔曼滤波器全是公式很简单
#include "modules/perception/obstacle/camera/common/visual_object.h" // 融合视觉对象
#include "modules/perception/obstacle/camera/interface/base_camera_filter.h" // 只是interface所以不用看

namespace apollo {
namespace perception {

class ObjectCameraExtendedKalmanFilter : public BaseCameraFilter {
 public:
  ObjectCameraExtendedKalmanFilter() : BaseCameraFilter() {}

  virtual ~ObjectCameraExtendedKalmanFilter() {}

  bool Init() override { return true; }

  bool Filter(const double timestamp,
              std::vector<std::shared_ptr<VisualObject>> *objects) override;

  std::string Name() const override;

  class ObjectFilter {
   public:
    ObjectFilter() {}

    ObjectFilter(const int track_id, const float last_timestamp) :
      track_id_(track_id), last_timestamp_(last_timestamp) {}

    int track_id_ = -1; // id应该是用来分辨不同物体的
    int lost_frame_cnt_ = 0; 
    float last_timestamp_ = 0.0f; // 时间要严格对应
    common::math::ExtendedKalmanFilter<float, 4, 3, 1> object_config_filter_;
  };

 private:
  const int kMaxKeptFrameCnt = 5; // 最大帧数目
 
  void GetState(const int track_id,
      const std::shared_ptr<VisualObject>& obj_ptr);  // 应该是从visualobject里面提取有用的state信息

  // @brief Predict step
  void Predict(const int track_id, const double timestamp); // 

  // @brief Update step
  void Update(const int track_id, const std::shared_ptr<VisualObject> &obj_ptr);

  ObjectFilter CreateObjectFilter(const int track_id, const float timestamp,
      const std::shared_ptr<VisualObject> &obj_ptr) const;

  common::math::ExtendedKalmanFilter<float, 4, 3, 1> InitObjectFilter(
      const float x, const float y, const float theta, const float v) const; // 估计就四个 xy角度速度

  Eigen::Matrix4f UpdateTransitionMatrix(const double theta, const double v,
      const double dt) const; // 角度速度单位时间

  std::unordered_map<int, ObjectFilter> tracked_filters_;

  // @brief Destroy old filters
  void Destroy();

  DISALLOW_COPY_AND_ASSIGN(ObjectCameraExtendedKalmanFilter);
};

}  // namespace perception
}  // namespace apollo

#endif
// MODULES_PERCEPTION_OBSTACLE_CAMERA_FILTER_OBJECT_CAMERA_KALMAN_FILTER_H_
