/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

// Convert 2D detections into 3D objects

#include "modules/perception/obstacle/camera/converter/geometry_camera_converter.h"

#include "modules/common/util/file.h"
#include "modules/perception/common/perception_gflags.h"

namespace apollo {
namespace perception {

using apollo::common::util::GetProtoFromFile;


// 初始化
// 读取config
// 读取相机内参和distortion
bool GeometryCameraConverter::Init() {
  if (!GetProtoFromFile(FLAGS_geometry_camera_converter_config, &config_)) {
    AERROR << "Cannot get config proto from file: "
           << FLAGS_geometry_camera_converter_config;
    return false;
  }

  if (!LoadCameraIntrinsics(config_.camera_intrinsic_file())) {
    AERROR << "Failed to get camera intrinsics: "
           << config_.camera_intrinsic_file();
    return false;
  }

  return true;
}

// 估计就是多个convert single了
bool GeometryCameraConverter::Convert(
    std::vector<std::shared_ptr<VisualObject>> *objects) {
  if (!objects) return false;

  for (auto &obj : *objects) {
    Eigen::Vector2f trunc_center_pixel = Eigen::Vector2f::Zero();
    CheckTruncation(obj, &trunc_center_pixel);
    CheckSizeSanity(obj);

    float deg_alpha = obj->alpha * 180.0f / M_PI;
    Eigen::Vector2f upper_left(obj->upper_left.x(), obj->upper_left.y());
    Eigen::Vector2f lower_right(obj->lower_right.x(), obj->lower_right.y());

    float distance = 0.0;
    Eigen::Vector2f mass_center_pixel = Eigen::Vector2f::Zero();
    if (obj->trunc_height < 0.25f) {
      // No truncation on 2D height
      ConvertSingle(obj->height, obj->width, obj->length, deg_alpha, upper_left,
                    lower_right, false, &distance, &mass_center_pixel);
    } else if (obj->trunc_width < 0.25f && obj->trunc_height > 0.25f) {
      // 2D height truncation and no width truncation
      ConvertSingle(obj->height, obj->width, obj->length, deg_alpha, upper_left,
                    lower_right, true, &distance, &mass_center_pixel);
    } else {
      // truncation on both sides
      // Give fix values for detected box with both side and bottom truncation
      distance = 10.0f;
      // Estimation of center pixel due to unknown truncation ratio
      mass_center_pixel = trunc_center_pixel;
    }

    obj->distance = distance;
    Eigen::Vector3f camera_ray = camera_model_.unproject(mass_center_pixel);
    DecideAngle(camera_ray, obj); // 通过质心再来看一下角度。

    // Center (3D Mass Center of 3D BBox)
    float scale = obj->distance / sqrt(camera_ray.x() * camera_ray.x() +
                                       camera_ray.y() * camera_ray.y() +
                                       camera_ray.z() * camera_ray.z());
    obj->center = camera_ray * scale;

    // Set 8 corner pixels
    SetBoxProjection(obj);
  }

  return true;
}

// 得到名称
std::string GeometryCameraConverter::Name() const {
  return "GeometryCameraConverter";
}

// 从yaml文件读取内参
bool GeometryCameraConverter::LoadCameraIntrinsics(
    const std::string &file_path) {
  YAML::Node node = YAML::LoadFile(file_path);

  Eigen::Matrix3f intrinsic_k;
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      int index = i * 3 + j;
      intrinsic_k(i, j) = node["K"][index].as<float>();
    }
  }

  Eigen::Matrix<float, 5, 1> intrinsic_d;
  for (int i = 0; i < 5; i++) {
    intrinsic_d(i, 0) = node["D"][i].as<float>();
  }

  float height = node["height"].as<float>();
  float width = node["width"].as<float>();
  camera_model_.set(intrinsic_k, width, height);
  camera_model_.set_distort_params(intrinsic_d);

  return true;
}


// single是什么暂时不明
// 输入：标定框左上角点，右下角点
bool GeometryCameraConverter::ConvertSingle(
    const float h, const float w, const float l, const float alpha_deg,
    const Eigen::Vector2f &upper_left, const Eigen::Vector2f &lower_right,
    bool use_width, float *distance, Eigen::Vector2f *mass_center_pixel) {
  
  // Target Goals: Projection target
  int pixel_width = static_cast<int>(lower_right.x() - upper_left.x()); // 宽度直接相减得到 
  int pixel_height = static_cast<int>(lower_right.y() - upper_left.y()); // 高度直接相减得到
  int pixel_length = pixel_height; // 很有意思，长度可以和高度相等，这是他的近似！！
  if (use_width) pixel_length = pixel_width; // 或者长度就等于宽度

  // Target Goals: Box center pixel
  Eigen::Matrix<float, 2, 1> box_center_pixel; // 标定框的中心点
  box_center_pixel.x() = (lower_right.x() + upper_left.x()) / 2.0f;
  box_center_pixel.y() = (lower_right.y() + upper_left.y()) / 2.0f;

  // Generate alpha rotated 3D template here. Corners in Camera space:
  // Bottom: FL, FR, RR, RL => Top: FL, FR, RR, RL
  float deg_alpha = alpha_deg;
  float h_half = h / 2.0f;
  float w_half = w / 2.0f;
  float l_half = l / 2.0f;

  std::vector<Eigen::Vector3f> corners;
  corners.resize(8); // 长度为8，8个点的3D标定框hwl暂时怎么来的不是很清楚。
  corners[0] = Eigen::Vector3f(l_half, h_half, w_half);
  corners[1] = Eigen::Vector3f(l_half, h_half, -w_half);
  corners[2] = Eigen::Vector3f(-l_half, h_half, -w_half);
  corners[3] = Eigen::Vector3f(-l_half, h_half, w_half);
  corners[4] = Eigen::Vector3f(l_half, -h_half, w_half);
  corners[5] = Eigen::Vector3f(l_half, -h_half, -w_half);
  corners[6] = Eigen::Vector3f(-l_half, -h_half, -w_half);
  corners[7] = Eigen::Vector3f(-l_half, -h_half, w_half);
  Rotate(deg_alpha, &corners);
  corners_ = corners; // 还需要知道视角
  pixel_corners_.clear(); // 图中八点
  pixel_corners_.resize(8); // 图中八点

  // Try to get an initial Mass center pixel and vector
  Eigen::Matrix<float, 3, 1> middle_v(0.0f, 0.0f, 20.0f); // 这个位置有什么特别之处？
  Eigen::Matrix<float, 2, 1> center_pixel = camera_model_.project(middle_v); // 相机project就是把3D空间点映射入图像位置

  float max_pixel_x = std::numeric_limits<float>::min();
  float min_pixel_x = std::numeric_limits<float>::max();
  float max_pixel_y = std::numeric_limits<float>::min();
  float min_pixel_y = std::numeric_limits<float>::max();
  for (size_t i = 0; i < corners.size(); ++i) {
    Eigen::Vector2f point_2d = camera_model_.project(corners[i] + middle_v); // 基本上就是把原来原点附近正方体沿着z向外20.0f
    min_pixel_x = std::min(min_pixel_x, point_2d.x()); // x的float最小可能值
    max_pixel_x = std::max(max_pixel_x, point_2d.x()); // x的float最大可能值
    min_pixel_y = std::min(min_pixel_y, point_2d.y()); // y...
    max_pixel_y = std::max(max_pixel_y, point_2d.y()); // y...
  }
  float relative_x =
      (center_pixel.x() - min_pixel_x) / (max_pixel_x - min_pixel_x);
  float relative_y =
      (center_pixel.y() - min_pixel_y) / (max_pixel_y - min_pixel_y);

  mass_center_pixel->x() =
      (lower_right.x() - upper_left.x()) * relative_x + upper_left.x(); //
  mass_center_pixel->y() = 
      (lower_right.y() - upper_left.y()) * relative_y + upper_left.y(); // 质心的求得方式还是有点怪
  Eigen::Matrix<float, 3, 1> mass_center_v =
      camera_model_.unproject(*mass_center_pixel); // 从图像中心点-> 图像质点-> 反推回质点的3D 点
  mass_center_v = MakeUnit(mass_center_v);  // 质点单位化 这是为啥？？

  // Distance search
  *distance =
      SearchDistance(pixel_length, use_width, mass_center_v, 0.1f, 150.0f);
  for (size_t i = 0; i < 1; ++i) {
    // Mass center search
    SearchCenterDirection(box_center_pixel, *distance, &mass_center_v,
                          mass_center_pixel);
    // Distance search
    *distance = SearchDistance(pixel_length, use_width, mass_center_v,
                               0.9f * (*distance), 1.1f * (*distance));
  }

  return true;
}

// 旋转函数就是把点按照观察角度旋转
void GeometryCameraConverter::Rotate(
    const float alpha_deg, std::vector<Eigen::Vector3f> *corners) const {
  Eigen::AngleAxisf yaw(alpha_deg / 180.0f * M_PI, Eigen::Vector3f::UnitY());
  Eigen::AngleAxisf pitch(0.0, Eigen::Vector3f::UnitX());
  Eigen::AngleAxisf roll(0.0, Eigen::Vector3f::UnitZ());
  Eigen::Matrix3f rotation = yaw.toRotationMatrix() * pitch.toRotationMatrix() *
                             roll.toRotationMatrix();

  Eigen::Matrix4f transform;
  transform.setIdentity();
  transform.block(0, 0, 3, 3) = rotation;

  for (auto &corner : *corners) {
    Eigen::Vector4f temp(corner.x(), corner.y(), corner.z(), 1.0f); // 齐次非齐次坐标变化
    temp = transform * temp;
    corner = Eigen::Vector3f(temp.x(), temp.y(), temp.z());
  }
}

// 这里输入的mass center已经是单位化的了。从上面convert single可以看出
// 
float GeometryCameraConverter::SearchDistance(
    const int pixel_length, const bool &use_width,
    const Eigen::Matrix<float, 3, 1> &mass_center_v, float close_d,
    float far_d) {
  float curr_d = 0.0f;  // 找到的距离
  int depth = 0;  // 找到的深度
  
  // 二分法找距离
  while (close_d <= far_d && depth < kMaxDistanceSearchDepth_) {
    curr_d = (far_d + close_d) / 2.0f;
    Eigen::Vector3f curr_p = mass_center_v * curr_d; // 这个公式是决定性的，当前中心点 = 质心点 * 距离 

    float min_p = std::numeric_limits<float>::max();
    float max_p = 0.0f;
    for (size_t i = 0; i < corners_.size(); ++i) {
      Eigen::Vector2f point_2d = camera_model_.project(corners_[i] + curr_p); // 同样的这里是将corners推到curr_p所在位置
                                                                              // 然后映射到图像位置
                                                                              // 所以这里就是遍历新中心位置所有角点
      float curr_pixel = 0.0f; 
      if (use_width) {
        curr_pixel = point_2d.x();
      } else {
        curr_pixel = point_2d.y();
      }

      min_p = std::min(min_p, curr_pixel); // 新中心位置八角点最小的x坐标
      max_p = std::max(max_p, curr_pixel); // 新中心位置八角点最大的x坐标
    }

    int curr_pixel_length = static_cast<int>(max_p - min_p); // 八点在图像的宽度上的两个端点x相减
    if (curr_pixel_length == pixel_length) { 
      break; // 这里很重要，映射完的真正八点要是宽度和图像上的宽度相同就算是找到了
    } else if (pixel_length < curr_pixel_length) {
      close_d = curr_d + 0.1f; // 二分还想内在缩小0.1
    } else {  // pixel_length > curr_pixel_length
      far_d = curr_d - 0.1f;
    }

    // Early break for 0.1m accuracy // 算不上是什么accuracy。。。不敢苟同
    float next_d = (far_d + close_d) / 2.0f;
    if (std::abs(next_d - curr_d) < 0.1f) {
      break;
    }

    ++depth; // 所以很明显这里的depth其实就是搜索的次数限制
  }

  // Only copy the last projection out
  Eigen::Vector3f curr_p = mass_center_v * curr_d;
  for (size_t i = 0; i < corners_.size(); ++i) {
    Eigen::Vector2f point_2d = camera_model_.project(corners_[i] + curr_p); // 知道实际的距离又知道八点的距离就可以求出图像八点。
    pixel_corners_[i] = point_2d;
  }

  return curr_d;
}

void GeometryCameraConverter::SearchCenterDirection(
    const Eigen::Matrix<float, 2, 1> &box_center_pixel, const float curr_d,
    Eigen::Matrix<float, 3, 1> *mass_center_v,
    Eigen::Matrix<float, 2, 1> *mass_center_pixel) const {
  int depth = 0;
  while (depth < kMaxCenterDirectionSearchDepth_) {
    Eigen::Matrix<float, 3, 1> new_center_v = *mass_center_v * curr_d;

    float max_pixel_x = std::numeric_limits<float>::min();
    float min_pixel_x = std::numeric_limits<float>::max();
    float max_pixel_y = std::numeric_limits<float>::min();
    float min_pixel_y = std::numeric_limits<float>::max();
    for (size_t i = 0; i < corners_.size(); ++i) {
      Eigen::Vector2f point_2d =
          camera_model_.project(corners_[i] + new_center_v);
      min_pixel_x = std::min(min_pixel_x, point_2d.x());
      max_pixel_x = std::max(max_pixel_x, point_2d.x());
      min_pixel_y = std::min(min_pixel_y, point_2d.y());
      max_pixel_y = std::max(max_pixel_y, point_2d.y());
    }

    Eigen::Matrix<float, 2, 1> current_box_center_pixel;
    current_box_center_pixel.x() = (max_pixel_x + min_pixel_x) / 2.0;
    current_box_center_pixel.y() = (max_pixel_y + min_pixel_y) / 2.0;

    // Update mass center
    *mass_center_pixel += box_center_pixel - current_box_center_pixel;
    *mass_center_v = camera_model_.unproject(*mass_center_pixel);
    *mass_center_v = MakeUnit(*mass_center_v); //质点单位化

    if (std::abs(mass_center_pixel->x() - box_center_pixel.x()) < 1.0 &&
        std::abs(mass_center_pixel->y() - box_center_pixel.y()) < 1.0) {
      break;
    }

    ++depth;
  }

  return;
}

// 把一个向量单位话 正则化。
Eigen::Matrix<float, 3, 1> GeometryCameraConverter::MakeUnit(
    const Eigen::Matrix<float, 3, 1> &v) const {
  Eigen::Matrix<float, 3, 1> unit_v = v;
  float to_unit_scale =
      std::sqrt(unit_v.x() * unit_v.x() + unit_v.y() * unit_v.y() +
                unit_v.z() * unit_v.z());
  unit_v /= to_unit_scale;
  return unit_v;
}

// 给各种对象的大小设定3D边界框不得小于一个最小框
void GeometryCameraConverter::CheckSizeSanity(
    std::shared_ptr<VisualObject> obj) const {
  if (obj->type == ObjectType::VEHICLE) {
    obj->length = std::max(obj->length, 3.6f);
    obj->width = std::max(obj->width, 1.6f);
    obj->height = std::max(obj->height, 1.5f);
  } else if (obj->type == ObjectType::PEDESTRIAN) {
    obj->length = std::max(obj->length, 0.5f);
    obj->width = std::max(obj->width, 0.5f);
    obj->height = std::max(obj->height, 1.7f);
  } else if (obj->type == ObjectType::BICYCLE) {
    obj->length = std::max(obj->length, 1.8f);
    obj->width = std::max(obj->width, 1.2f);
    obj->height = std::max(obj->height, 1.5f);
  } else {
    obj->length = std::max(obj->length, 0.5f);
    obj->width = std::max(obj->width, 0.5f);
    obj->height = std::max(obj->height, 1.5f);
  }
}

// 如果对象在图像太靠近边缘需要截取图像2D框
void GeometryCameraConverter::CheckTruncation(
    std::shared_ptr<VisualObject> obj,
    Eigen::Matrix<float, 2, 1> *trunc_center_pixel) const {
  auto width = camera_model_.get_width();
  auto height = camera_model_.get_height();

  // Ad-hoc 2D box truncation binary determination
  if (obj->upper_left.x() < 30.0f || width - 30.0f < obj->lower_right.x()) {
    obj->trunc_width = 0.5f;

    if (obj->upper_left.x() < 30.0f) {
      trunc_center_pixel->x() = obj->upper_left.x();
    } else {
      trunc_center_pixel->x() = obj->lower_right.x();
    }
  }

  if (obj->upper_left.y() < 30.0f || height - 30.0f < obj->lower_right.y()) {
    obj->trunc_height = 0.5f;
    trunc_center_pixel->x() =
        (obj->upper_left.x() + obj->lower_right.x()) / 2.0f;
  }

  trunc_center_pixel->y() = (obj->upper_left.y() + obj->lower_right.y()) / 2.0f;
}

// 距离由高度决定？？
float GeometryCameraConverter::DecideDistance(
    const float distance_h, const float distance_w,
    std::shared_ptr<VisualObject> obj) const {
  float distance = distance_h;
  return distance;
}

// 决定距离和角度暂时不是很懂
// 重新再来优化一下角度其实就是
void GeometryCameraConverter::DecideAngle(
    const Eigen::Vector3f &camera_ray,
    std::shared_ptr<VisualObject> obj) const {
  float beta = std::atan2(camera_ray.x(), camera_ray.z());

  // Orientation is not reliable in these cases (DL model specific issue)
  if (obj->distance > 50.0f || obj->trunc_width > 0.25f) {
    obj->theta = -1.0f * M_PI_2;
    obj->alpha = obj->theta - beta;
    if (obj->alpha > M_PI) {
      obj->alpha -= 2 * M_PI;
    } else if (obj->alpha < -M_PI) {
      obj->alpha += 2 * M_PI;
    }
  } else {  // Normal cases
    float theta = obj->alpha + beta;
    if (theta > M_PI) {
      theta -= 2 * M_PI;
    } else if (theta < -M_PI) {
      theta += 2 * M_PI;
    }
    obj->theta = theta;
  }
}

// 就是把box从 VisualObject 取出来到pixel_corners_
void GeometryCameraConverter::SetBoxProjection(
    std::shared_ptr<VisualObject> obj) const {
  obj->pts8.resize(16);
  if (obj->trunc_width < 0.25f && obj->trunc_height < 0.25f) {  // No truncation
    for (int i = 0; i < 8; i++) {
      obj->pts8[i * 2] = pixel_corners_[i].x();
      obj->pts8[i * 2 + 1] = pixel_corners_[i].y();
    }
  }
}

}  // namespace perception
}  // namespace apollo
