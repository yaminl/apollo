/******************************************************************************
 * 2D信息怎么变成3D的信息就在这个文件里面，看来这里是相当重要的
 * 
 *****************************************************************************/
// 2D信息怎么变成3D的信息就在这个文件里面，看来这里是相当重要的
// Convert 2D detections into 3D objects with physical position in camera space

#ifndef MODULES_PERCEPTION_OBSTACLE_CAMERA_CONVERTER_GEOMETRY_H_
#define MODULES_PERCEPTION_OBSTACLE_CAMERA_CONVERTER_GEOMETRY_H_

#include <algorithm>
#include <cmath>
#include <limits>
#include <memory>
#include <string>
#include <vector>

#include "Eigen/Geometry"
#include "opencv2/opencv.hpp"
#include "yaml-cpp/yaml.h"

#include "modules/perception/proto/geometry_camera_converter_config.pb.h" // 定义了相机参数真正有用的就是一个yaml的位置也就是相机内参的位置

#include "modules/common/log.h"
#include "modules/perception/obstacle/base/types.h"
#include "modules/perception/obstacle/camera/common/camera.h" // 相机内参
#include "modules/perception/obstacle/camera/common/visual_object.h" // 融合参数
#include "modules/perception/obstacle/camera/interface/base_camera_converter.h" // 父类只是一个纯虚界面

namespace apollo {
namespace perception {

class GeometryCameraConverter : public BaseCameraConverter {
 public:
  GeometryCameraConverter() : BaseCameraConverter() {}

  virtual ~GeometryCameraConverter() {}

  bool Init() override;

  // @brief: Convert 2D detected objects into physical 3D objects
  // @param [in/out] objects : detected object lists, added 3D position and
  // orientation
  bool Convert(std::vector<std::shared_ptr<VisualObject>> *objects) override;

  std::string Name() const override;

 private:
  bool LoadCameraIntrinsics(const std::string &file_path);

  bool ConvertSingle(const float h, const float w, const float l,
                     const float alpha_deg, const Eigen::Vector2f &upper_left,
                     const Eigen::Vector2f &lower_right, bool use_width,
                     float *distance, Eigen::Vector2f *mass_center_pixel);

  void Rotate(const float alpha_deg,
              std::vector<Eigen::Vector3f> *corners) const;

  float SearchDistance(const int pixel_length, const bool &use_width,
                       const Eigen::Matrix<float, 3, 1> &mass_center_v,
                       float close_d, float far_d);

  void SearchCenterDirection(
      const Eigen::Matrix<float, 2, 1> &box_center_pixel, const float curr_d,
      Eigen::Matrix<float, 3, 1> *mass_center_v,
      Eigen::Matrix<float, 2, 1> *mass_center_pixel) const;

  Eigen::Matrix<float, 3, 1> MakeUnit(
      const Eigen::Matrix<float, 3, 1> &v) const;

  // Physical Size sanity check based on type
  void CheckSizeSanity(std::shared_ptr<VisualObject> obj) const;

  // Check truncation based on 2D box position
  void CheckTruncation(std::shared_ptr<VisualObject> obj,
                       Eigen::Matrix<float, 2, 1> *trunc_center_pixel) const;

  // Choose distance based on 2D box width or height
  float DecideDistance(const float distance_h, const float distance_w,
                       std::shared_ptr<VisualObject> obj) const;

  void DecideAngle(const Eigen::Vector3f &camera_ray,
                   std::shared_ptr<VisualObject> obj) const;

  void SetBoxProjection(std::shared_ptr<VisualObject> obj) const;
    
  CameraDistort<float> camera_model_;  // 相机内参，主要是为了2d转3d必备
  std::vector<Eigen::Vector3f> corners_; // 3d标定框
  std::vector<Eigen::Vector2f> pixel_corners_; // 2d标定框这个应该是yolo给的
  static const int kMaxDistanceSearchDepth_ = 10; // 这里不太明白是啥，关键kmax算法是啥？
  static const int kMaxCenterDirectionSearchDepth_ = 5;

  geometry_camera_converter_config::ModelConfigs config_; //这个是设置

  DISALLOW_COPY_AND_ASSIGN(GeometryCameraConverter); // 不允许复制和赋值
};

// Register plugin
REGISTER_CAMERA_CONVERTER(GeometryCameraConverter); // 写入寄存器？？这个以后再说吧

}  // namespace perception
}  // namespace apollo

#endif  // MODULES_PERCEPTION_OBSTACLE_CAMERA_CONVERTER_GEOMETRY_H_
