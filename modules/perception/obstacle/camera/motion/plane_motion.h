/******************************************************************************
 * 对于连串汽车运动信息，速度加速度，以及空间位置的处理，主要使用了buffer和list
 * 来做
 *****************************************************************************/

#ifndef MODULES_PERCEPTION_OBSTACLE_CAMERA_MOTION_PLANE_MOTION_H_
#define MODULES_PERCEPTION_OBSTACLE_CAMERA_MOTION_PLANE_MOTION_H_

#include <cmath>
#include <cstdio>
#include <list>
#include <memory>

#include "Eigen/Dense"
#include "Eigen/Eigen"
#include "Eigen/Geometry"
#include "modules/perception/lib/base/mutex.h"
#include "modules/perception/obstacle/base/object_supplement.h"

namespace apollo {
namespace perception {

// plane是平面的意思吗，平面运动吗？这里所说的运动其实是运动状态的记录
class PlaneMotion {
 public:
  explicit PlaneMotion(int s);

  ~PlaneMotion(void);
  enum { ACCUM_MOTION = 0, ACCUM_PUSH_MOTION, PUSH_ACCUM_MOTION, RESET };

 private:
  std::list<VehicleStatus> raw_motion_queue_; // VehicleStatus结构体主要记录汽车速度和角速度,还有空间位置的4D矩阵motiontype
  MotionBufferPtr mot_buffer_;  // MotionBuffer是一个VehicleStatus的buffer
  Mutex mutex_;  // 锁
  int buffer_size_;  // buffer大小
  int time_increment_;     // the time increment units in motion input
  float time_difference_;  // the time difference for each buffer input
  MotionType mat_motion_sensor_ = MotionType::Identity(); // 这里就是一个单位矩阵
  // motion matrix of accumulation through high sampling CAN+IMU input sequence,CAN貌似是一种通讯协议
  bool is_3d_motion_; // 3D运动
  
  // 这里的motion matrix其实是单位时间的瞬时的变换矩阵，需要从单位时间，角速度，速度来求出
  // 至于inverse是为什么我至今还不明白
  void generate_motion_matrix(
      VehicleStatus *vehicledata);  // generate inverse motion
    
  
  void accumulate_motion(double start_time, double end_time); 
  void update_motion_buffer(const VehicleStatus &vehicledata,
                            const double pre_image_timestamp,
                            const double image_timestamp);

 public:
  void cleanbuffer() {
    if (mot_buffer_ != nullptr) {
      mot_buffer_->clear();
      mot_buffer_ = nullptr;
    }

    mat_motion_sensor_ = MotionType::Identity(); // 这里可以看出mat_motion_sensor记录sensor的运动，刚开始是单位矩阵
  }


  void set_buffer_size(int s) {
    cleanbuffer(); // 如果里面有东西会先清空
    buffer_size_ = s;
    // mot_buffer_.reserve(buffer_size_);
    if (mot_buffer_ == nullptr) {
      mot_buffer_ = std::make_shared<MotionBuffer>(buffer_size_);
    } else {
      mot_buffer_->set_capacity(buffer_size_);
    }
  }

  // void init(int s) { set_buffer_size(s); }

  //   void add_new_motion(VehicleStatus *vehicledata, float motion_time_dif,
  //                      int motion_operation_flag);

  void add_new_motion(double pre_image_timestamp, double image_timestamp,
                      int motion_operation_flag, VehicleStatus *vehicledata);

  MotionBuffer get_buffer();
  bool find_motion_with_timestamp(double timestamp, VehicleStatus *vs); 
  bool is_3d_motion() const { return is_3d_motion_; }
};

}  // namespace perception
}  // namespace apollo

#endif  // MODULES_PERCEPTION_OBSTACLE_CAMERA_MOTION_PLANE_MOTION_H_
