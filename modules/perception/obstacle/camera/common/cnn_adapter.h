/******************************************************************************
 * 这个文件就是卷积神经网络c++的实现，就是工程里面的正向推倒c++代码
 *****************************************************************************/

#ifndef MODULES_PERCEPTION_OBSTACLE_CAMERA_COMMON_CNN_ADAPTER_H_
#define MODULES_PERCEPTION_OBSTACLE_CAMERA_COMMON_CNN_ADAPTER_H_

#include <string>
#include <vector>

#include "caffe/caffe.hpp"

#include "modules/perception/cuda_util/util.h"

namespace apollo {
namespace perception {

class CNNAdapter {
 public:
  virtual void forward() = 0;

  virtual boost::shared_ptr<caffe::Blob<float>> get_blob_by_name(
      const std::string &name) = 0;

  virtual bool init(const std::vector<std::string> &input_names,
                    const std::vector<std::string> &output_names,
                    const std::string &proto_file,
                    const std::string &weight_file, int gpu_id,
                    const std::string &model_root = "") = 0;

  virtual bool shape(const std::string &name, std::vector<int> *res) = 0;
  virtual bool reshape_input(const std::string &name,
                             const std::vector<int> &shape) = 0;
};

class CNNCaffe : public CNNAdapter {
 public:
  CNNCaffe() = default;

  bool init(const std::vector<std::string> &input_names,
            const std::vector<std::string> &output_names,
            const std::string &proto_file, const std::string &weight_file,
            int gpu_id, const std::string &model_root = "") override;

  void forward() override;

  boost::shared_ptr<caffe::Blob<float>> get_blob_by_name(
      const std::string &name) override;

  bool shape(const std::string &name, std::vector<int> *res) override {
    auto blob = get_blob_by_name(name);
    if (blob == nullptr) {
      return false;
    }
    *res = blob->shape();
    return true;
  }

  bool reshape_input(const std::string &name,
                     const std::vector<int> &shape) override {
    auto blob = get_blob_by_name(name);
    if (blob == nullptr) {
      return false;
    }
    blob->Reshape(shape);
    forward();
    return true;
  }

 private:
  boost::shared_ptr<caffe::Net<float>> net_;
  int gpu_id_ = 0;
};

}  // namespace perception
}  // namespace apollo

#endif  // MODULES_PERCEPTION_OBSTACLE_CAMERA_COMMON_CNN_ADAPTER_H_
