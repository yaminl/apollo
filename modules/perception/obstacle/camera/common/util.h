/******************************************************************************
 * 这个文件主要是把visualobejct读取和写入文件并对类型进行一些简单的转换处理内容
 * 并不多
 *****************************************************************************/

#ifndef MODULES_PERCEPTION_OBSTACLE_CAMERA_COMMON_UTIL_H_
#define MODULES_PERCEPTION_OBSTACLE_CAMERA_COMMON_UTIL_H_

#include <fstream>
#include <iostream>
#include <iterator>
#include <memory>
#include <string>
#include <vector>

#include "google/protobuf/io/coded_stream.h"
#include "google/protobuf/io/gzip_stream.h"
#include "google/protobuf/io/zero_copy_stream_impl.h"
#include "google/protobuf/text_format.h"
#include "opencv2/opencv.hpp"

#include "modules/perception/obstacle/camera/common/visual_object.h"

namespace apollo {
namespace perception {

extern std::vector<cv::Scalar> color_table;

// Color definition for CV
// 规定一些常用颜色
const cv::Scalar COLOR_WHITE = cv::Scalar(255, 255, 255);
const cv::Scalar COLOR_GREEN = cv::Scalar(0, 255, 0);
const cv::Scalar COLOR_BLUE = cv::Scalar(255, 0, 0);
const cv::Scalar COLOR_YELLOW = cv::Scalar(0, 255, 255);
const cv::Scalar COLOR_RED = cv::Scalar(0, 0, 255);
const cv::Scalar COLOR_BLACK = cv::Scalar(0, 0, 0);

// 把visual_object视觉对象读取和写入文件
bool LoadVisualObjectFromFile(
    const std::string &file_name,
    std::vector<std::shared_ptr<VisualObject>> *visual_objects);

bool WriteVisualObjectToFile(
    const std::string &file_name,
    std::vector<std::shared_ptr<VisualObject>> *visual_objects);

bool LoadGTfromFile(const std::string &gt_path,
                    std::vector<std::shared_ptr<VisualObject>> *visual_objects);


std::string GetTypeText(ObjectType type);

ObjectType GetObjectType(const std::string &type_text);

}  // namespace perception
}  // namespace apollo

#endif  // MODULES_PERCEPTION_OBSTACLE_CAMERA_COMMON_UTIL_H_
