/******************************************************************************
 * 暂时没有搞清楚这个映射是用来干嘛的
 *****************************************************************************/

#ifndef MODULES_PERCEPTION_OBSTACLE_CAMERA_COMMON_PROJECTOR_H_
#define MODULES_PERCEPTION_OBSTACLE_CAMERA_COMMON_PROJECTOR_H_

#include <Eigen/Core>

#include <string>
#include <vector>

namespace apollo {
namespace perception {

class BaseProjector {
 public:
  virtual bool project(std::vector<float>* feature) = 0;
};

// 矩阵映射，给一个权重，感觉只是处理一下矩阵的意思并不知道具体是用来干嘛的暂时
class MatrixProjector : public BaseProjector {
 public:
  explicit MatrixProjector(std::string weight_file);

  bool project(std::vector<float>* feature) override;

 private:
  Eigen::MatrixXf matrix_;
};

}  // namespace perception
}  // namespace apollo

#endif  // MODULES_PERCEPTION_OBSTACLE_CAMERA_COMMON_PROJECTOR_H_
