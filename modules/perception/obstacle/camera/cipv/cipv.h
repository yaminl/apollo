/******************************************************************************
 * cipv到底是啥的缩写？？看了一下官网是路径中最近的物体
 * 那就是关于找距离跟踪的意思吧。
 *****************************************************************************/

#ifndef MODULES_PERCEPTION_OBSTACLE_CAMERA_CIPV_H_
#define MODULES_PERCEPTION_OBSTACLE_CAMERA_CIPV_H_

#include <array>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "Eigen/Dense"
#include "Eigen/Eigen"
#include "Eigen/Geometry"

#include "modules/common/configs/vehicle_config_helper.h"
#include "modules/perception/obstacle/base/object.h"
#include "modules/perception/obstacle/camera/common/lane_object.h"
#include "modules/perception/obstacle/camera/lane_post_process/common/type.h"

namespace apollo {
namespace perception {

struct CipvOptions {
  float velocity = 5.0f;  // 速度
  float yaw_rate = 0.0f;  // 就是垂直汽车的那个坐标轴这个应该就是角速度
  float yaw_angle = 0.0f; // 当前角度
};

const float MAX_DIST_OBJECT_TO_LANE_METER = 20.0f;  // 最大偏离路线距离
const float MAX_VEHICLE_WIDTH_METER = 5.0f;  // 最大车宽度
const float EPSILON = 1.0e-6f;  // 极小值
const std::size_t DROPS_HISTORY_SIZE = 100;  // 最大忽略历史数目
const std::size_t MAX_OBJECT_NUM = 100;  // 最大对象数目
const std::size_t MAX_ALLOWED_SKIP_OBJECT = 10; // 最大忽略对象数目

// TODO(All) averatge image frame rate should come from other header file.
const float AVERAGE_FRATE_RATE = 0.1f;  // 看注释都看不懂

class Cipv {
  // Member functions
 public:
  //    friend class ::adu::perception::OnlineCalibrationService;
  Cipv(void);
  virtual ~Cipv(void);

  virtual bool Init();
  virtual std::string Name() const;
    
  // 确定最近的对象所以需要知道车道线和设置
  // Determine CIPV among multiple objects
  bool DetermineCipv(const LaneObjectsPtr lane_objects,
                     const CipvOptions &options,
                     std::vector<std::shared_ptr<Object>> *objects);
  
  // 搜集drops，不知道drops是什么，tailgating是尾行，跟车的意思
  // Collect drops for tailgating
  bool CollectDrops(const MotionBuffer &motion_buffer,
                    std::vector<std::shared_ptr<Object>> *objects);

 private:
 
  // 点到直线距离
  // Distance from a point to a line segment
  bool DistanceFromPointToLineSegment(const Point2Df &point,
                                      const Point2Df &line_seg_start_point,
                                      const Point2Df &line_seg_end_point,
                                      float *distance);

  // 在多个对象找最近的？？
  // Determine CIPV among multiple objects
  bool GetEgoLane(const LaneObjectsPtr lane_objects, EgoLane *egolane_image,
                  EgoLane *egolane_ground, bool *b_left_valid,
                  bool *b_right_valid);

  // Elongate lane line
  bool ElongateEgoLane(const LaneObjectsPtr lane_objects,
                       const bool b_left_valid, const bool b_right_valid,
                       const float yaw_rate, const float velocity,
                       EgoLane *egolane_image, EgoLane *egolane_ground);

  // Get closest edge of an object in image cooridnate
  bool FindClosestEdgeOfObjectImage(const std::shared_ptr<Object> &object,
                                    const EgoLane &egolane_image,
                                    LineSegment2Df *closted_object_edge);

  // Get closest edge of an object in ground cooridnate
  bool FindClosestEdgeOfObjectGround(const std::shared_ptr<Object> &object,
                                     const EgoLane &egolane_ground,
                                     LineSegment2Df *closted_object_edge);

  // Check if the distance between lane and object are OK
  bool AreDistancesSane(const float distance_start_point_to_right_lane,
                        const float distance_start_point_to_left_lane,
                        const float distance_end_point_to_right_lane,
                        const float distance_end_point_to_left_lane);

  // Check if the object is in the lane in image space
  bool IsObjectInTheLaneImage(const std::shared_ptr<Object> &object,
                              const EgoLane &egolane_image);
  // Check if the object is in the lane in ego-ground space
  //  |           |
  //  | *------*  |
  //  |         *-+-----*
  //  |           |  *--------* <- closest edge of object
  // *+------*    |
  //  |           |
  // l_lane     r_lane
  bool IsObjectInTheLaneGround(const std::shared_ptr<Object> &object,
                               const EgoLane &egolane_ground);

  // Check if the object is in the lane in ego-ground space
  bool IsObjectInTheLane(const std::shared_ptr<Object> &object,
                         const EgoLane &egolane_image,
                         const EgoLane &egolane_ground);

  // Check if a point is left of a line segment
  bool IsPointLeftOfLine(const Point2Df &point,
                         const Point2Df &line_seg_start_point,
                         const Point2Df &line_seg_end_point);

  // Make a virtual lane line using a reference lane line and its offset
  // distance
  bool MakeVirtualLane(const LaneLine &ref_lane_line, const float yaw_rate,
                       const float offset_distance,
                       LaneLine *virtual_lane_line);

  float VehicleDynamics(const uint32_t tick, const float yaw_rate,
                        const float velocity, const float time_unit, float *x,
                        float *y);
  // Make a virtual lane line using a yaw_rate
  bool MakeVirtualEgoLaneFromYawRate(const float yaw_rate, const float velocity,
                                     const float offset_distance,
                                     LaneLine *left_lane_line,
                                     LaneLine *right_lane_line);

  // transform point to another using motion
  bool TranformPoint(const Eigen::VectorXf& in,
                     const MotionType& motion_matrix,
                     Eigen::Vector3d* out);
  // Member variables
  bool b_image_based_cipv_ = false;
  int32_t debug_level_ = 0;
  float time_unit_ = 0.0f;
  common::VehicleParam vehicle_param_;

  const float EGO_CAR_WIDTH_METER;
  const float EGO_CAR_LENGTH_METER;
  const float EGO_CAR_MARGIN_METER;
  const float EGO_CAR_VIRTUAL_LANE;
  const float EGO_CAR_HALF_VIRTUAL_LANE;

  std::map<int, size_t> object_id_skip_count_;
  std::map<int, boost::circular_buffer<std::pair<float, float>>>
    object_trackjectories_;
  std::map<int, std::vector<double>> object_timestamps_;
};

}  // namespace perception
}  // namespace apollo

#endif  // MODULES_PERCEPTION_OBSTACLE_CAMERA_CIPV_H_
