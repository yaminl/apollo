/******************************************************************************
 * 这是camera converter的一个纯虚类interface
 * camera converter就是如何从2Dyolo输出转化为3D信息的重要的类！
 * 实际的实现需要认真阅读。
 *****************************************************************************/

// The base class of converting 2D detections into 3D objects

#ifndef MODULES_PERCEPTION_OBSTACLE_CAMERA_INTERFACE_BASE_CAMERA_CONVERTER_H_
#define MODULES_PERCEPTION_OBSTACLE_CAMERA_INTERFACE_BASE_CAMERA_CONVERTER_H_

#include <memory>
#include <string>
#include <vector>

#include "Eigen/Core"
#include "opencv2/opencv.hpp"

#include "modules/common/macro.h"
#include "modules/perception/lib/base/registerer.h"
#include "modules/perception/obstacle/camera/common/visual_object.h"

namespace apollo {
namespace perception {

class BaseCameraConverter {
 public:
  BaseCameraConverter() {}
  virtual ~BaseCameraConverter() {}

  virtual bool Init() = 0;

  // @brief: Convert 2D detected objects into physical 3D objects
  // @param [in/out]: detected object lists, added 3D position and orientation
  virtual bool Convert(std::vector<std::shared_ptr<VisualObject>>* objects) = 0;

  virtual std::string Name() const = 0;

 private:
  DISALLOW_COPY_AND_ASSIGN(BaseCameraConverter);
};

REGISTER_REGISTERER(BaseCameraConverter);
#define REGISTER_CAMERA_CONVERTER(name) \
  REGISTER_CLASS(BaseCameraConverter, name)

}  // namespace perception
}  // namespace apollo

#endif  // MODULES_PERCEPTION_OBSTACLE_CAMERA_INTERFACE_BASE_CAMERA_CONVERTER_H_
