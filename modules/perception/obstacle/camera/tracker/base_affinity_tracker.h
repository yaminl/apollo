/******************************************************************************
 * tracker tracker核心是对roi pooling里面的样子进行一个kernelized correlation 
 * 滤波器滤波
 *****************************************************************************/

#ifndef MODULES_PERCEPTION_OBSTACLE_CAMERA_TRACKER_BASE_AFFINITY_TRACKER_H_
#define MODULES_PERCEPTION_OBSTACLE_CAMERA_TRACKER_BASE_AFFINITY_TRACKER_H_

#include <opencv2/opencv.hpp>
#include <limits>
#include <vector>

#include "Eigen/Core"

namespace apollo {
namespace perception {

// 一个被track的物体
class Tracked {
 public:
  cv::Rect box_;   // 2D bounding box  2D标定框
  int track_id_;   // unique tracking id 
  int detect_id_;  // -1 means unmatched but kept
  double first_timestamp_;  // 从什么时候开始
  double last_timestamp_;  // 上一个时间
  int last_frame_idx_;  // 上一帧的id

  // DLF: Deep Learning ROI Pooling features from detection
  std::vector<float> features_; // 看来连续track主要是roi pooling feature来决定的

  // 3D position observed in camera space
  Eigen::Vector3f center_ = Eigen::Vector3f::Zero(); // 中心点

  // KCF
  bool kcf_set_ = false; // Kernelized Correlation Filters 核心相关滤波器
  std::vector<cv::Mat> x_f_;  //一系列帧
  cv::Mat alpha_f_; // 不太懂
};

// 刚被yolo的状态
class Detected {
 public:
  cv::Rect box_;
  int detect_id_;

  // DLF: Deep Learning ROI Pooling features from detection
  std::vector<float> features_;

  // 3D position observed in camera space
  Eigen::Vector3f center_ = Eigen::Vector3f::Zero();
};


class BaseAffinityTracker {
 public:
  BaseAffinityTracker() {}

  virtual ~BaseAffinityTracker() {}

  virtual bool Init() = 0;

  // @brief Choose some entries in the matrix to do computation for affinity
  //
  // Skip the entry with
  // 1. 0 or negative affinity score
  // 2. High score (>= 9.0)
  //
  // Input must be regular and of the size in the following calls
  // (There must be the same number of entries in each row of the matrix)
  //
  virtual bool SelectEntries(
      const std::vector<std::vector<float>> &prev_matrix) {
    if (prev_matrix.empty()) return true;

    int row = prev_matrix.size();
    if (!row) return true;
    int col = prev_matrix[0].size();

    selected_entry_matrix_.clear();
    selected_entry_matrix_ =
        std::vector<std::vector<bool>>(row, std::vector<bool>(col, false));
    for (int i = 0; i < row; ++i) {
      for (int j = 0; j < col; ++j) {
        if (9.0f > prev_matrix[i][j] &&
            prev_matrix[i][j] > std::numeric_limits<float>::epsilon()) {
          selected_entry_matrix_[i][j] = true;
        }
      }
    }

    return true;
  }

  // @brief Set the selection matrix to full entries
  virtual bool SelectFull(const int row, const int col) {
    selected_entry_matrix_.clear();
    selected_entry_matrix_ =
        std::vector<std::vector<bool>>(row, std::vector<bool>(col, true));
    return true;
  }

  // @brief Get affinity_matrix between tracked objs and detected objs
  // rows: tracked objects, cols: detected objects
  // affinity_matrix[i][j]: score for the affinity between i_th tracked obj and
  // j_th detected obj
  virtual bool GetAffinityMatrix(
      const cv::Mat &img, const std::vector<Tracked> &tracked,
      const std::vector<Detected> &detected,
      std::vector<std::vector<float>> *affinity_matrix) = 0;

  // @brief Update information used in tracked objects, for the next frame
  virtual bool UpdateTracked(const cv::Mat &img,
                             const std::vector<Detected> &detected,
                             std::vector<Tracked> *tracked) = 0;

 protected:
  // @brief Matrix for selecting which entries need calculation
  std::vector<std::vector<bool>> selected_entry_matrix_;
};

}  // namespace perception
}  // namespace apollo

#endif  // MODULES_PERCEPTION_OBSTACLE_CAMERA_TRACKER_BASE_AFFINITY_TRACKER_H_
