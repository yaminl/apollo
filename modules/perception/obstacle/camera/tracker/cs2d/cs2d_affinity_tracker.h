/******************************************************************************
 * 主要是得看看kcl是怎么具体实现的。
 *****************************************************************************/

#ifndef MODULES_PERCEPTION_OBSTACLE_CAMERA_TRACKER_CS2D_AFFINITY_TRACKER_H
#define MODULES_PERCEPTION_OBSTACLE_CAMERA_TRACKER_CS2D_AFFINITY_TRACKER_H

#include <cmath>
#include <limits>
#include <unordered_map>
#include <vector>

#include "modules/perception/obstacle/camera/tracker/base_affinity_tracker.h" 

namespace apollo {
namespace perception {

class CS2DAffinityTracker : public BaseAffinityTracker {
 public:
  CS2DAffinityTracker() : BaseAffinityTracker() {}

  virtual ~CS2DAffinityTracker() {}

  bool Init() override;
  bool GetAffinityMatrix(
      const cv::Mat &img, const std::vector<Tracked> &tracked,
      const std::vector<Detected> &detected,
      std::vector<std::vector<float>> *affinity_matrix) override;

  bool UpdateTracked(const cv::Mat &img, const std::vector<Detected> &detected,
                     std::vector<Tracked> *tracked) override;

 private:
  float sz_lim_ = 0.5f;         // max 2d box scale change 最大大小变化
  float pos_range_ = 1.3f;      // max 2D center change, based on 2d box size 最大位置变化
  float center_range_ = 10.0f;  // max diff in meter between unfiltered 3d pos 最大实际位置变化不超过10米
};

}  // namespace perception
}  // namespace apollo

#endif  // MODULES_PERCEPTION_OBSTACLE_CAMERA_TRACKER_CS2D_AFFINITY_TRACKER_H
