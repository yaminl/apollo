/******************************************************************************
 * 仅仅定义了scalar type
 *****************************************************************************/

#ifndef MODULES_PERCEPTION_OBSTACLE_CAMERA_LANE_POST_PROCESS_BASE_TYPE_H_
#define MODULES_PERCEPTION_OBSTACLE_CAMERA_LANE_POST_PROCESS_BASE_TYPE_H_

namespace apollo {
namespace perception {

typedef float ScalarType;

}  // namespace perception
}  // namespace apollo

#endif  // MODULES_PERCEPTION_OBSTACLE_CAMERA_LANE_POST_PROCESS_BASE_TYPE_H_
