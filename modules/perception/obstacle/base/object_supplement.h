/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef MODULES_PERCEPTION_OBSTACLE_BASE_OBJECT_SUPPLEMENT_H_
#define MODULES_PERCEPTION_OBSTACLE_BASE_OBJECT_SUPPLEMENT_H_

#include <memory>
#include <string>
#include <vector>

#include "Eigen/Core"
#include "boost/circular_buffer.hpp"
#include "opencv2/opencv.hpp"

#include "modules/perception/obstacle/base/types.h"

namespace apollo {
namespace perception {

// 这个文件是为了object对象的信息补充。
// 之所以会使用补充信息，我猜测这和python面向对象编程的感觉很像
// apollo可能是先写了一个object作为一切事物的出发点
// 加上不同的补充信息构成了不同的东西。

// 跟踪信息：其实就是3D刚体变换所以用了4D矩阵来实现
struct TrackStateVars {
  Eigen::Matrix4d process_noise = Eigen::Matrix4d::Identity();
  // Eigen::Matrix4d measure_noise = Eigen::Matrix4f::Identity();
  Eigen::Matrix4d trans_matrix = Eigen::Matrix4d::Identity();
  bool initialized_ = false;
};

// 激光雷达帧的补充信息，其实也就只有3D信息
struct alignas(16) LidarFrameSupplement {
  static TrackStateVars state_vars;
};
// 激光雷达帧的补充信息的智能指针
typedef std::shared_ptr<LidarFrameSupplement> LidarFrameSupplementPtr;
typedef std::shared_ptr<const LidarFrameSupplement>
    LidarFrameSupplementConstPtr;

// 普通雷达的补充信息，这里不是帧而是硬件本身
// 包括：
// 1. 距离和角度范围
// 2. 相对径向速度，相对切向速度，绝对径向速度
struct alignas(16) RadarSupplement {
  RadarSupplement();
  ~RadarSupplement();
  RadarSupplement(const RadarSupplement& rhs);
  RadarSupplement& operator=(const RadarSupplement& rhs);
  void clone(const RadarSupplement& rhs);

  // distance
  float range = 0.0f;
  // x -> forward, y -> left
  float angle = 0.0f;
  float relative_radial_velocity = 0.0f;
  float relative_tangential_velocity = 0.0f;
  float radial_velocity = 0.0f;
};

// 雷达补充信息的只能指针
typedef std::shared_ptr<RadarSupplement> RadarSupplementPtr;
typedef std::shared_ptr<const RadarSupplement> RadarSupplementConstPtr;

// 雷达帧的补充信息，其实除去构造函数之外还是只有3D信息
struct alignas(16) RadarFrameSupplement {
  RadarFrameSupplement();
  ~RadarFrameSupplement();
  RadarFrameSupplement(const RadarFrameSupplement& rhs);
  RadarFrameSupplement& operator=(const RadarFrameSupplement& rhs);
  void clone(const RadarFrameSupplement& rhs);
  static TrackStateVars state_vars;
};

// 雷达帧补充信息的智能指针，这里这么多智能指针估计是为了后面生成的时候做memory management忘记中文叫啥了
typedef std::shared_ptr<RadarFrameSupplement> RadarFrameSupplementPtr;
typedef std::shared_ptr<const RadarFrameSupplement>
    RadarFrameSupplementConstPtr;

// 相机帧补充信息：
// 非常呼之欲出：
// 1. 深度图，原始图像
// 2. 标识图label
// 3. 道路图
// 4. source_topic暂时不知道是什么东西
// 5. 上面每个帧都有的位置信息
struct alignas(16) CameraFrameSupplement {
  CameraFrameSupplement();
  ~CameraFrameSupplement();
  CameraFrameSupplement(const CameraFrameSupplement& rhs);
  CameraFrameSupplement& operator=(const CameraFrameSupplement& rhs);
  void clone(const CameraFrameSupplement& rhs);

  cv::Mat depth_map;
  cv::Mat label_map;
  cv::Mat lane_map;
  cv::Mat img_src;
  std::string source_topic;
  static TrackStateVars state_vars;
};

// 相机帧补充信息的智能指针
typedef std::shared_ptr<CameraFrameSupplement> CameraFrameSupplementPtr;

typedef std::shared_ptr<const CameraFrameSupplement>
    CameraFrameSupplementConstPtr;

// 相机补充信息：
// 1. 左上角右上角，这里是那个东西的不是非常清楚
// 2. 跟踪id
// 3. 写不下去了，以后看到怎么回事才能理解这里是什么东西
struct alignas(16) CameraSupplement {
  CameraSupplement();
  ~CameraSupplement();
  CameraSupplement(const CameraSupplement& rhs);
  CameraSupplement& operator=(const CameraSupplement& rhs);
  void clone(const CameraSupplement& rhs);

  // upper-left corner: x1, y1
  Eigen::Vector2d upper_left;
  // lower-right corner: x2, y2
  Eigen::Vector2d lower_right;
  // local track id
  int local_track_id = -1;

  // 2Dto3D, pts8.resize(16), x, y...
  std::vector<float> pts8;

  // front box upper-left corner: x1, y1
  Eigen::Vector2d front_upper_left;
  // front box  lower-right corner: x2, y2
  Eigen::Vector2d front_lower_right;

  // front box upper-left corner: x1, y1
  Eigen::Vector2d back_upper_left;
  // front box  lower-right corner: x2, y2
  Eigen::Vector2d back_lower_right;

  std::vector<float> object_feature;

  // this is `alpha` angle from KITTI: Observation angle of object,
  // ranging [-pi..pi]
  double alpha = 0.0;
};

// 相机补充信息的智能指针
typedef std::shared_ptr<CameraSupplement> CameraSupplementPtr;
typedef std::shared_ptr<const CameraSupplement> CameraSupplementConstPtr;

// motion也是一个刚体变换
typedef Eigen::Matrix4f MotionType;

// 汽车信息：角速度速度，时间
struct alignas(16) VehicleStatus {
  float roll_rate = 0;
  float pitch_rate = 0;
  float yaw_rate = 0;
  float velocity = 0;
  float velocity_x = 0;
  float velocity_y = 0;
  float velocity_z = 0;
  double time_ts = 0;     // time stamp
  double time_d = 0;      // time stamp difference in image
  MotionType motion = MotionType::Identity();  // Motion Matrix
};

// 汽车的信息buffer
typedef boost::circular_buffer<VehicleStatus> MotionBuffer;
// 汽车信息buffer对应只能指针
typedef std::shared_ptr<MotionBuffer> MotionBufferPtr;
typedef std::shared_ptr<const MotionBuffer> MotionBufferConstPtr;

// 汽车3D信息基本上和刚才的差不多
struct alignas(16) Vehicle3DStatus {
  float yaw_delta;  // azimuth angle change
  float pitch_delta;
  float roll_delta;
  float velocity_x;          // east
  float velocity_y;          // north
  float velocity_z;          // up
  float time_t;              // time stamp
  float time_d;              // time stamp difference in image
  Eigen::Matrix4f motion3d;  // 3-d Motion Matrix
};

// 对应buffer和智能指针
typedef boost::circular_buffer<Vehicle3DStatus> Motion3DBuffer;
typedef std::shared_ptr<Motion3DBuffer> Motion3DBufferPtr;
typedef std::shared_ptr<const Motion3DBuffer> Motion3DBufferConstPtr;

}  // namespace perception
}  // namespace apollo

#endif  // MODULES_PERCEPTION_OBSTACLE_BASE_OBJECT_SUPPLEMENT_H_
